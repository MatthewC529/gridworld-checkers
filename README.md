GridWorld-Checkers
==================

Created for AP Computer Science A in 2014

Implements GUI Code, the Java Reflection API and relatively advanced implementations to not only modify the GridWorld GUI but also to optimize the Movement of Pieces and the time-consuming processes. Multi-Threaded Movement and Unit Management (And GUI Management) is also implemented.
