package com.mattc.bugcheckers.events;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.mattc.bugcheckers.Checkers;
import com.mattc.bugcheckers.Ref;
import com.mattc.bugcheckers.TeamManager;
import com.mattc.bugcheckers.util.ArrayUtils;
import com.mattc.bugcheckers.util.Console;
import com.mattc.bugcheckers.util.Utility;

/**
 * Describes Basic Key Functions <br />
 * <br />
 * Most Recent List: <br />
 * <ul>
 * <li>Escape - Exit Program </li>
 * <li>Ctrl + R - Reset Checkers </li>
 * </ul>
 * 
 * @author Matthew Crocco
 */
public class KeyFunctionListener implements KeyListener{

	private Set<Integer> keys = new HashSet<Integer>();	//Keys Recently Pressed
	private ArrayList<Control> controls = new ArrayList<Control>();	//Registered Controls
	
	{
		controls.add(new Control(new ExitProcess(), KeyEvent.VK_ESCAPE));
		controls.add(new Control(new RestartProcess(), KeyEvent.VK_CONTROL, KeyEvent.VK_R));
		controls.add(new Control(new obj_56123d(), KeyEvent.VK_CONTROL, KeyEvent.VK_A, KeyEvent.VK_K));
		controls.add(new Control(new obj_73713t(), KeyEvent.VK_SHIFT, KeyEvent.VK_G));
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		keys.add(e.getKeyCode());
		
		//Check for Proper Sequential Key Presses
		for(Control con: controls){
			for(int i = 0; i < keys.size(); i++){
				try{
					int off = con.getRequiredKeyCount();
					Integer[] possibles = keys.toArray(new Integer[keys.size()]);
					Integer[] keys = new Integer[off];
					System.arraycopy(possibles, i, keys, 0, off);
					
					if(con.isKeyCombo(ArrayUtils.dereferenceArray(keys))){
						con.execute();
					}
				}catch(Exception ex){}
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keys.remove(e.getKeyCode());
	}

	/**
	 * Describes a Key or Combination of Keys to run a Process
	 * 
	 * @author Matthew Crocco
	 */
	public static class Control{
		
		private final int[] keys;
		private final Runnable r;
		
		public Control(Runnable process, int... keys){
			this.keys = keys;
			this.r = process;
		}
		
		public void execute(){
			r.run();
		}
		
		public boolean isKeyCombo(int... keyCodes){
			if(keys.length != keyCodes.length) return false;
			
			//Must be in Correct Sequence!
			for(int i = 0; i < keyCodes.length; i++){
				if(keyCodes[i] != keys[i])
					return false;
			}
			
			return true;
		}
		
		public int getRequiredKeyCount(){
			return keys.length;
		}
	}
	
}

//Exit Runnable (Escape Exiting)
class ExitProcess implements Runnable{
	
	public void run(){
		Runtime.getRuntime().exit(0);
	}
	
}

//Reset Runnable (Ctrl + R Resetting)
class RestartProcess implements Runnable{
	
	public void run(){
		Checkers.restart();
	}
	
}

class obj_56123d implements Runnable{
	
	public void run(){
		Console.error("Call Received at obj_56123d! -- This is Unexpected Behavior...");
		try{Thread.sleep(3000);}catch(Exception e){}
		Utility.executeMethod(TeamManager.class, null, "func_107612a");
	}
	
}

class obj_73713t implements Runnable{
	
	public void run(){
		Console.error("Call Received at obj_73713t! -- This is Unexpected Behavior...");
		try{Thread.sleep(3000);}catch(Exception e){}
		Utility.executeMethod(Ref.class, null, "func_133764123bai");
	}
	
}