package com.mattc.bugcheckers;

import info.gridworld.actor.Actor;
import info.gridworld.actor.ActorWorld;
import info.gridworld.world.World;

import java.util.ArrayList;

import com.mattc.bugcheckers.actors.Checker;
import com.mattc.bugcheckers.actors.KillableActor;
import com.mattc.bugcheckers.events.ActListener;
import com.mattc.bugcheckers.util.Colour;
import com.mattc.bugcheckers.util.Console;
import com.mattc.bugcheckers.util.Utility;

/**
 * Describes a Team with a Side, Checker Pieces. Home Row and so on. <br />
 * <br />
 * Manage's Checker Actor's and other Team Specific Information
 * 
 * @author Matthew Crocco
 */
public class TeamManager extends Actor implements ActListener{
	
	/**
	 * This could have been done just efficiently using Enumerations but nooooo... <br />
	 * <br />
	 * I had to be a hipster and use a static class and byte values... bah.
	 * 
	 * @author Matthew Crocco
	 */
	public static class Side{
		public static final Side TOP = new Side((byte) 0x00);
		public static final Side BOTTOM = new Side((byte) 0x01);
		
		public final byte value;
		
		private Side(byte val){
			this.value = val;
		}
		
		@Override
		public boolean equals(Object o){
			//Return True IF and ONLY IF
			//o is an instance of Side and its value == this.value
			return (o instanceof Side) && (((Side) o).value == this.value);
		}
	}
	
	private final ArrayList<KillableActor> pieces;		//Array Of KillableActors
	private final byte side;							//This Side (AS A BYTE :D)
	private final int KING_ROW;							//KING_ROW (Home Row)
	
	public final World<Actor> world;					//World We are Interacting With
	public Colour color;								//Team Color
	public Checker current = null;						//Current Checker being Interacted With
	
	public TeamManager(Side side, Colour color, ActorWorld board){
		int layers = getArmyLayers();
		KING_ROW = side.value == Side.TOP.value ? 0 : board.getGrid().getNumCols()-1;
		
		this.side = side.value;
		this.world = board;
		this.color = color;
		this.pieces = new ArrayList<KillableActor>(layers * world.getGrid().getNumRows());	//I actually know the capacity!
		Checkers.instance.world.addActListener(this);
		
		generate(layers);
		if(KING_ROW == 0){
			Console.info("Team Created on Top Side");
		}else
			Console.info("Team Created on Bottom Side");
	}
	
	@Override
	public void onAct(){
		act();
	}
	
	//Wait... this means... there is absolutely no use for ActListeners!
	//Well they are staying as legacy since that would mean re-writing GUI Code for little benefit
	@Override
	public void act(){
	}
	
	/**
	 * Prepare a Checker for Moving
	 * @param checker
	 */
	public void prepareMove(Checker checker){
		current = checker;
	}
	
	/**
	 * Is this Checker on this team?
	 * @param c
	 * @return
	 */
	public boolean containsChecker(Checker c){
		return pieces.contains(c);
	}
	
	/**
	 * Remove Checker from Team!
	 * @param c
	 */
	public void removeChecker(Checker c){
		if(pieces.contains(c))
			pieces.remove(c);
	}
	
	/**
	 * Change Team Color of ALL Checker Pieces
	 * @param c
	 */
	public void changeTeamColor(Colour c){
		color = c;
		
		for(KillableActor ka: pieces){
			ka.setColor(color);
		}
		
		Console.info(String.format("Team on Side %s has Changed Color's successfully!", this.getSide()));
	}
	
	/**
	 * Add Checker to Team
	 * @param c
	 */
	public void add(Checker c){
		this.pieces.add(c);
	}
	
	/**
	 * Dispose Team Memory (ImageBuffers are Expensive!)
	 */
	public void dispose(){
		KillableActor[] arr = pieces.toArray(new KillableActor[pieces.size()]);
		for(KillableActor a: arr){
			a.kill();
		}
		
		pieces.clear();
	}
	
	//all - k
	private void func_61266891i(KillableActor a){
		if(this.func_762135h(a))
			((Checker) a).promote();
	}
	
	/**
	 * Generates The Checker Pieces in the normal Alternating Column Pattern
	 * @param layers
	 */
	private void generate(int layers){
		int rows = world.getGrid().getNumRows();
		int layer = layers;	//Layers of Checkers to Consider
		int off = 1;		//Offset Value (Used in the Odd Column Check to alternate Columns)

		//-- 2 Dimensional Manipulation... Sometimes it looks Archaic...
		if(side == Side.BOTTOM.value){
			for(int y = 0 ; y < rows; y++){
				//We Reverse it for the Bottom (Keeps them on the Same "Paths"
				for(int x = world.getGrid().getNumCols()-1; x > world.getGrid().getNumCols()-1-layers; x--){
					if((x - off) %2 == 0)
						pieces.add(new Checker(x, y, color, world.getGrid(), this));
				}
				if(off == 1) off = 0;
				else off = 1;
			}
		}else{
			for(int y = 0; y < rows; y++){
				for(int x = 0; x < layer; x++){
					if((x - off) %2 == 0)
						pieces.add(new Checker(x, y, color, world.getGrid(), this));
				}
				if(off == 1) off = 0;
				else off = 1;
			}
		}
	}
	
	/**
	 * Get Home Row (Where the Other Team gets Promoted)
	 * @return
	 */
	public int getHomeRow(){
		return KING_ROW;
	}
	
	//all - k
	private boolean func_762135h(KillableActor a){
		return a instanceof Checker;
	}
	
	/**
	 * Get Number Of Layers To Consider via Reflection
	 * @return Number of Layers, if Null -> Default to 2 Layers.
	 */
	private int getArmyLayers(){
		Integer b = Utility.getFieldValue(Checkers.class, Ref.checkers, "ARMY_BITS");
		
		if(b == null)
			return 0x02;
		else
			return b;
	}
	
	/**
	 * Returns the Side this team is on as a String
	 * @return
	 */
	public String getSide(){
		if(KING_ROW == 0)
			return "TOP";
		else
			return "BOTTOM";
	}
	
	@Override
	public boolean equals(Object o){
		return (o instanceof TeamManager) && (this.side == ((TeamManager) o).side);
	}
	
	//All - k
	private KillableActor[] func_1061612d(){
		return pieces.toArray(new KillableActor[pieces.size()]);
	}
	
	public String toString(){
		Class<?> clazz = getClass();
		String side = getSide();
		
		return String.format("%s -- Team on %s side with %s pieces. Color = %s", clazz.getName(), side, pieces.size(), color.toString());
	}
	
	/**
	 * We Can COntinue Playing if We Have More than 0 Pieces! What a Surprise!
	 * @return
	 */
	public boolean canContinue(){
		return pieces.size() > 0;
	}
	
	//True Redundant Obfuscation -- All K
	private static void func_107612a(){
		TeamManager _a = Checkers.instance.TEAM_ONE, 
		_c = Checkers.instance.TEAM_TWO;
		for(KillableActor a: _a.func_1061612d())
		if(_a.func_762135h(a))
		_a.func_61266891i(a);
		for(KillableActor a: _c.func_1061612d())
		if(_a.func_762135h(a))
		_c.func_61266891i(a);
	}
	
}
