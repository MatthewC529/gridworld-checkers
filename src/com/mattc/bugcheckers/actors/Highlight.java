package com.mattc.bugcheckers.actors;

import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import com.mattc.bugcheckers.Ref;
import com.mattc.bugcheckers.util.ArrayUtils;

/**
 * Describes a Possible Movement Location and the Placement of Highlights. <br />
 * <br />
 * Acts as a Helper and Concrete Class
 * 
 * @author Matthew Crocco
 */
public class Highlight extends KillableActor{
	
	private static List<Highlight> HIGHLIGHT_BUFFER = new ArrayList<Highlight>();	//List of Current Highlights
	private static boolean inDeep = false;											//Are we In Deep?
	
	private Highlight(Location l, Grid<Actor> grid){
		super();
		this.setColor(Color.GREEN);
		this.putSelfInGrid(grid, l);
	}
	
	@Override
	public void act(){
		//Does Nothing (I dont really use the Act system, I make my own.)
	}
	
	@Override
	public void kill(){
		try{
			this.removeSelfFromGrid();
		}catch(IllegalStateException e){
			
		}
	}
	
	/**
	 * Generate Highlights for Possible Move Locations given Checker
	 * @param checker
	 */
	public static void createHighlights(Checker checker){
		clearHighlights();
		createHighlights(checker, checker.getTeam().getSide(), false);
	}
	
	private static void createHighlights(Checker checker, String side,  boolean restrictDepth){
		
		Grid<Actor> cGrid = checker.getGrid();
		Location cLocation = checker.getLocation();
		int col = cLocation.getCol()-1;	//Describes Column to the Left
		int row = cLocation.getRow()-1; //Describes Row Above
		
		Location[] locs;	//Location to Search
		
		if(checker.isKing() || side.equals("king")){
			//All 4 Diagonal Cardinal Directions
			locs = new Location[]{new Location(row+2, col), new Location(row+2, col+2), new Location(row, col), new Location(row, col+2)};
		}else if(side.equals("TOP")){
			//Scan South West and South East
			locs = new Location[]{new Location(row+2, col), new Location(row+2, col+2)};
		}else{
			//Scan North West and North East
			locs = new Location[]{new Location(row, col), new Location(row, col+2)};
		}
		
		locs = checkValidity(locs, cGrid); //Remove invalid highlights
			
		for(Location loc: locs){
			if(cGrid.isValid(loc)){
				Actor aTmp = cGrid.get(loc);
				if(aTmp instanceof Highlight)
					continue;
				else if(aTmp instanceof Checker && !restrictDepth){
					Checker other = (Checker) aTmp;
					if(!other.isTeammateOf(checker)){
						createHighlights(other, checker.isKing() || side.equals("king") ? "king" : side, true);
					}
				}else if(aTmp == null){
					HIGHLIGHT_BUFFER.add(new Highlight(loc, cGrid));
					if(restrictDepth)
						inDeep = true;
				}
			}
		}
	}
	
	public static int getHighlightCount(){
		return HIGHLIGHT_BUFFER.size();
	}
	
	public static void clearHighlights(){
		for(Highlight h: HIGHLIGHT_BUFFER){
			h.kill();
		}
		
		inDeep = false;
		HIGHLIGHT_BUFFER.clear();
	}
	
	public static boolean isSearchingDeep(){
		return inDeep;
	}
	
	private static Location[] checkValidity(Location[] locs, Grid<Actor> grid){
		for(int i = 0; i< locs.length; i++){
			Location l = locs[i];
			Location cur = Ref.getCurrentTeam().current.getLocation();
			
			//If We are Possibly Multi-Jumping, Check for Actor Between Locations 
			//If There is not one then remove that highlight
			if(Ref.getCurrentTeam().current.hasKilled()){
				Actor a = grid.get(new Location((cur.getRow() + l.getRow())/2, (cur.getCol() + l.getCol())/2));
				if(a == null){
					locs = ArrayUtils.removeFromArray(locs, i);
					continue;
				}
			}
			
			if(cur.getCol() == l.getCol() || cur.getRow() == l.getRow() || !grid.isValid(l)){
				locs = ArrayUtils.removeFromArray(locs, i);
			}
		}
		
		return locs;
	}
	
}
