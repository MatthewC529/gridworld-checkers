package com.mattc.bugcheckers.actors;

import com.mattc.bugcheckers.util.Colour;

/**
 * Describes a Checker that is Omni-Directional (and Special [CROWN!])
 * 
 * @author Matthew Crocco
 */
public class CheckerKing extends Checker{

	public enum Direction{
		
		NONE(0,0),
		NORTHWEST(-1, -1),
		NORTHEAST(+1, -1),
		SOUTHWEST(-1, +1),
		SOUTHEAST(+1, +1);
		
		public final int rOff;
		public final int cOff;
		
		private Direction(int c, int r){
			cOff = c;
			rOff = r;
		}
	}
	
	private Direction dir = Direction.NONE;
	
	protected CheckerKing(Checker c){
		super(c.getLocation().getRow(), c.getLocation().getCol(), new Colour(c.getColor()), c.getGrid(), c.getTeam());
		this.getTeam().add(this);
		this.king();
	}
	
	public Direction getCardinalDirection(){
		return this.dir;
	}
	
	public void setCardinalDirection(Direction dir){
		this.dir = dir;
	}
	
}
