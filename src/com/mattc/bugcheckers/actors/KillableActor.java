package com.mattc.bugcheckers.actors;

import info.gridworld.actor.Actor;

/**
 * Describes an Actor that is removable using the Kill Method
 * 
 * @author Matthew Crocco
 */
public abstract class KillableActor extends Actor {

	public abstract void kill();
	
}
