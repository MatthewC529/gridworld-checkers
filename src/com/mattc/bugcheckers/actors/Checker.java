package com.mattc.bugcheckers.actors;

import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import com.mattc.bugcheckers.Ref;
import com.mattc.bugcheckers.TeamManager;
import com.mattc.bugcheckers.util.Colour;
import com.mattc.bugcheckers.util.Console;

/**
 * Describes the Basic Piece Of Checkers (Or Draughts in Any Other Country)
 * @author Matthew Crocco
 *
 */
public class Checker extends KillableActor {

	private TeamManager team;				//Team I Belong To... 
	private boolean isKing = false;			//My Future...
	private boolean murderer = false;		//The Cost...
	
	/**
	 * I Was Brilliant and Screwed up the X and Y... But it works, just have to learn it the hard way :P <br />
	 * The TeamManager handles it anyway.
	 * @param x
	 * @param y
	 * @param c
	 * @param grid
	 * @param team
	 */
	public Checker(int x, int y, Colour c, Grid<Actor> grid, TeamManager team){
		super();
		this.setColor(c);
		this.team = team;
		this.putSelfInGrid(grid, new Location(x, y));
		Console.info(getClass().getSimpleName() + " Created At " + this.getLocation().toString()); 
	}
	
	/**
	 * Warning: Doesnt Actually Cause a Move To Occur
	 */
	public void move(){
		team.prepareMove(this);
	}
	
	/**
	 * Warning: Does Actually Cause a Move To Occur
	 * @param newLoc
	 */
	public void jump(Location newLoc){
		Location cur = this.getLocation();
		
		float dR = (cur.getRow() + newLoc.getRow()) / 2;
		float dC = (cur.getCol() + newLoc.getCol()) / 2;
		
		//If Moved Diagonally (IT SHOULD!)
		if(dR % 1 == 0 && dC % 1 == 0){
			Actor a = this.getGrid().get(new Location((int)dR, (int)dC));
			if(a instanceof Checker){
				Checker other = (Checker) a;
				if(!other.isTeammateOf(this)){		//If Not On Same Team
					other.kill();					//Commit Sin...
					murderer = true;				//Become my worst fears...
					Ref.killOccurred = true;		//They all know...
				}
			}
		}
		
		this.moveTo(newLoc);	//ACTUALLY MOVE :D
	}
	
	/**
	 * Determines if a Jump can Still Occur post-mortem
	 * @return
	 */
	public boolean canJumpAfterKill(){
		if(!Ref.killOccurred) return false;	//I didnt kill anyone...
		
		Highlight.clearHighlights();
		Highlight.createHighlights(this);
		//If There are Highlights and we Have Hit an Actor
		if(Highlight.getHighlightCount() > 0 && Highlight.isSearchingDeep()){
			Highlight.clearHighlights();
			return true;
		}else{
			Highlight.clearHighlights();
			return false;
		}
	}
	
	public TeamManager getTeam(){
		return team;
	}
	
	public void toggleKilled(){
		murderer = false;
	}
	
	public boolean hasKilled(){
		return murderer;
	}
	
	public boolean isKing(){
		return isKing;
	}
	
	//My Destiny Reached...
	protected void king(){
		if(this instanceof CheckerKing)
			isKing = true;
	}
	
	/**
	 * Replaces The Current Checker with a CheckerKing at this Location
	 */
	public void promote(){
		Console.info("Checker Promoted at " + this.getLocation() + " to King for " + this.team.getSide() + " team!"); 
		if(isKing) return;
		
		CheckerKing king = new CheckerKing(this);
		if(Ref.getCurrentTeam().current == this)
			Ref.getCurrentTeam().current = king;
		team.removeChecker(this);
	}
	
	@Override
	public void act(){
		//And Yet I do nothing....
	}
	
	/**
	 * Ensures no Friendly Fire
	 * @param c
	 * @return
	 */
	public boolean isTeammateOf(Checker c){
		return team.containsChecker(this) && team.containsChecker(c);
	}
	
	@Override
	public void removeSelfFromGrid(){
		if(this.getGrid() == null) return;
		super.removeSelfFromGrid();
		team.removeChecker(this);
	}
	
	public void changeColor(String name){
		Colour c = Colour.findColour(name);
		team.changeTeamColor(c);
	}
	
	/**
	 * Colour Described between 0.0 and 1.0
	 * @param r
	 * @param g
	 * @param b
	 * @param a
	 */
	public void changeColor(float r, float g, float b, float a){
		Colour c = new Colour(r, g, b, a);
		team.changeTeamColor(c);
	}
	
	public void changeColorWithHex(String hex){
		Colour c = Colour.getColour(hex);
		team.changeTeamColor(c);
	}
	
	//Dividends of Valor
	@Override
	public void kill(){
		if(this.getGrid() == null) return;
		Console.info("Checker Killed at " + this.getLocation().toString());
		this.removeSelfFromGrid();
	}
	
}
