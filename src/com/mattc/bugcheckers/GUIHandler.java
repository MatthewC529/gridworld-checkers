package com.mattc.bugcheckers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;

import com.mattc.bugcheckers.events.ActListener;
import com.mattc.bugcheckers.util.Colour;
import com.mattc.bugcheckers.util.Console;
import com.mattc.bugcheckers.util.Utility;
import com.mattc.bugcheckers.world.CheckersWorld;

/**
 * 
 * I HIGHLY Discourage Reading this Class... GUI Code is my messiest Code <br />
 * <br />
 * This Helper Class is used to display Dialog's and Pop-ups as necessary and to allocate and de-allocate <br />
 * resources as necessary.
 * 
 * @author Matthew Crocco
 */
public final class GUIHandler {

	private GUIHandler(){}
	
	public static void displayHelp(){
		JTextArea list = new JTextArea();
		list.setFont(new Font("Times New Roman", Font.BOLD , 16));
		JDialog dialog = new JDialog(Ref.frame, "GridWorld Checkers Help");
		dialog.setLayout(new BorderLayout());
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		StringBuilder sb = new StringBuilder();
		sb.append("=====================================================\n");
		sb.append("  GridWorld Checkers " + Ref.VERSION + " by Matthew Crocco \n");
		sb.append("  Created for OHS AP Computer Science Class         \n");
		sb.append("=====================================================\n");
		sb.append("\tAbout Checkers: http://goo.gl/1SxVcU              \n");
		sb.append("=====================================================\n");
		sb.append("\tFUNCTION: \t|\tKEYSTROKE                          \n");
		sb.append("=====================================================\n");
		sb.append("\tExit      \t|\tPress Escape                       \n");
		sb.append("\tRestart   \t|\tPress Ctrl + R                     \n");
		sb.append("=====================================================\n");
		
		list.setText(sb.toString());
		dialog.add(list, BorderLayout.CENTER);
		
		dialog.setBounds(200, 200, 700, 400);
		dialog.setResizable(false);
		dialog.setVisible(true);
	}
	
	/**
	 * Display List of Active ActListeners
	 * @param instance
	 */
	public static void displayActListenerList(Checkers instance){
		JDialog dialog = new JDialog(Ref.frame, "Act Listeners", true);
		dialog.setLayout(new FlowLayout());
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		
		JTextArea text = new JTextArea();
		text.setEditable(false);
		
		JScrollPane pane = new JScrollPane(text);
		
		//-- Actual List Generation
		CheckersWorld world = (CheckersWorld) instance.world;
		ActListener[] arr = world.getActListeners();
		int i = 0;
		for(ActListener al : arr){
			String msg = text.getText();
			msg += String.format("%s. %s -- %s\n", ++i, al.getClass().getName(), al.toString());
			text.setText(msg);
		}
		//--
		
		dialog.add(pane);
		
		dialog.setBounds(200, 200, text.getPreferredSize().width+10, text.getPreferredSize().height+30);
		dialog.setResizable(false);
		dialog.setVisible(true);
	}
	
	/**
	 * Displays the Options for Changing Game Parameters to be done at Reset <br / > 
	 * <br />
	 * WARNING!: It is all loaded into memory, it is recommended you reset as soon as changing parameters to avoid <br />
	 * conflicting or mixed up values!
	 */
	public static void displayModificationOptions(){
		
		//-- Components
		final JButton ok = new JButton("Confirm (Needs Restart)");
		final JButton close = new JButton("Cancel (Revert Changes)");
		final JButton revert = new JButton("Revert (To Original Settings)");
		final JSpinner gridSize = new JSpinner(new SpinnerNumberModel(Ref.GRID_SIZE, 8, 12, 1));
		final JSpinner layerDepth = new JSpinner(new SpinnerNumberModel(Ref.ARMY_BITS, 2, 4, 1));
		final DefaultListModel<Colour> cModel = new DefaultListModel<Colour >();
		final JList<Colour> cListOne = new JList<Colour>(cModel);
		final JList<Colour> cListTwo = new JList<Colour>(cModel);
		JScrollPane paneOne = new JScrollPane(cListOne);
		JScrollPane paneTwo = new JScrollPane(cListTwo);
		final JDialog dialog = new JDialog(Ref.frame, "Debug Options", true);
		dialog.setLayout(new FlowLayout(FlowLayout.CENTER, 40, 10));
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		//--
		
		//Action on Confirmation
		ok.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				disableAll();
				Ref.REF_GRID_SIZE = Ref.GRID_SIZE;
				Ref.GRID_SIZE = (Integer) gridSize.getValue();
				Ref.ARMY_BITS = (Integer) layerDepth.getValue();
				resize();
				setColor();
				enableAll();
			}
			
			public void setColor(){
				if(cListOne.getSelectedValue() == null){
					Ref.TOP_COLOR = Ref.DEF_TCOLOR;
				}else
					Ref.TOP_COLOR = cListOne.getSelectedValue();
				if(cListTwo.getSelectedValue() == null){
					Ref.BOT_COLOR = Ref.DEF_BCOLOR;
				}else
					Ref.BOT_COLOR = cListTwo.getSelectedValue();
			}
			
			public void resize(){
				Ref.REF_FRAME_DIM = Ref.FRAME_DIM;
				Ref.FRAME_DIM = Ref.DEF_FRAME_DIM;
				int diff = Ref.GRID_SIZE - Ref.REF_GRID_SIZE;
				int fix = diff * 48;
				Console.debug(String.format("Resizing with Diff=%s, Fix=%s", diff, fix ));
				
				Ref.FRAME_DIM.width += fix;
				Ref.FRAME_DIM.height += fix;
			}
			
			public void disableAll(){
				ok.setEnabled(false);
				close.setEnabled(false);
				gridSize.setEnabled(false);
				layerDepth.setEnabled(false);
				cListOne.setEnabled(false);
				cListTwo.setEnabled(false);
			}
			
			public void enableAll(){
				ok.setEnabled(true);
				close.setEnabled(true);
				gridSize.setEnabled(true);
				layerDepth.setEnabled(true);
				cListOne.setEnabled(true);
				cListTwo.setEnabled(true);
			}
		});
		
		// Action on Cancellation
		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.dispose();
			}
		});
		
		// Action on Revert
		revert.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				disableAll();
				Ref.REF_GRID_SIZE = Ref.GRID_SIZE;
				Ref.GRID_SIZE = Ref.DEF_GSIZE;
				Ref.ARMY_BITS = Ref.DEF_ABITS;
				resize();
				Ref.TOP_COLOR = Ref.DEF_TCOLOR;
				Ref.BOT_COLOR = Ref.DEF_BCOLOR;
				enableAll();
			}
			
			public void resize(){
				Ref.FRAME_DIM = Ref.DEF_FRAME_DIM;
				int diff = Ref.GRID_SIZE - Ref.REF_GRID_SIZE;
				int fix = diff * 48;
				Console.debug(String.format("Resizing with Diff=%s, Fix=%s", diff, fix ));
				
				Ref.FRAME_DIM.width += fix;
				Ref.FRAME_DIM.height += fix;
			}
			
			public void disableAll(){
				ok.setEnabled(false);
				close.setEnabled(false);
				gridSize.setEnabled(false);
				layerDepth.setEnabled(false);
				cListOne.setEnabled(false);
				cListTwo.setEnabled(false);
			}
			
			public void enableAll(){
				ok.setEnabled(true);
				close.setEnabled(true);
				gridSize.setEnabled(true);
				layerDepth.setEnabled(true);
				cListOne.setEnabled(true);
				cListTwo.setEnabled(true);
			}
		});
		
		//Populate Colour List... Had to add a special Colour Case :P
		GUIHandler.<Colour>populateListFrom(cModel, Colour.class); 
		
		//-- Actually Adding the Components to the GUI
		dialog.add(new JLabel("Grid Dimensions: "));
		dialog.add(gridSize);
		dialog.add(new JLabel("Army Layers: "));
		dialog.add(layerDepth);
		dialog.add(new JLabel("Top Team Color: "));
		dialog.add(paneOne);
		dialog.add(new JLabel("Bottom Team Color: "));
		dialog.add(paneTwo);
		dialog.add(ok);
		dialog.add(close);
		dialog.add(revert);
		//--
		
		layerDepth.setSize(gridSize.getSize());
		
		dialog.setBounds(200, 200, 600, 500);
		dialog.setResizable(false);
		dialog.setVisible(true);
	}
	
	/**
	 * Victory Dialog
	 * @param team
	 */
	public static void callVictory(TeamManager team){
		JButton reset = new JButton("Reset Checkers");
		
		JDialog dialog = new JDialog(Ref.frame);
		dialog.setLayout(new BorderLayout());
		dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		
		JTextArea text = new JTextArea();
		text.setEditable(false);
		
		JScrollPane pane = new JScrollPane(text);
		
		text.setText(String.format("Congratulations %s Team!"
								  +"\n"
								  +"You have defeated your opponent! Will another rise to challenge you?", team.getSide()));
		
		reset.setSize(text.getPreferredSize().width, reset.getHeight());
		reset.addActionListener(new ResetAction(dialog));
		
		dialog.add(pane, BorderLayout.CENTER);
		dialog.add(reset, BorderLayout.SOUTH);
		dialog.setBounds(200, 200, text.getPreferredSize().width+100, text.getHeight() + reset.getHeight() + 100);
		dialog.setResizable(false);
		dialog.setVisible(true);
	}
	
	public static void setMessage(String msg){
		Checkers.instance.world.setMessage(msg);
	}
	
	/**
	 * Populates List via Reflection on all Field Values
	 * @param model
	 * @param clazz
	 */
	public static <T> void populateListFrom(DefaultListModel<T> model, Class<T> clazz){
		
		Field[] fields = clazz.getFields();
		
		if(clazz == Colour.class){
			for(Field f: fields){
				try{
					if(f.getType() == Color.class){
						Color color = (Color) f.get(null);
						Colour c = new Colour(color,f.getName());
						String name = c.toString();
						name = name.substring(name.indexOf("="), name.indexOf("Hex", name.indexOf("="))).trim();
						if(Utility.capsCheck(name)) continue;
						model.addElement((T)c);
					}
				}catch(Exception e){
					Console.exception(e);
				}
			}
		}else{
			for(Field f: fields){
				try{
					if(f.get(null).getClass().equals(clazz)){
						model.addElement((T)f.get(null));
					}
				}catch(Exception e){}
			}
		}
	}
	
}

/**
 * Describes Action Conducted when Reset is Clicked
 * 
 * @author Matthew Crocco
 */
class ResetAction implements ActionListener{

	JDialog dialog;
	
	public ResetAction(JDialog dialog){
		this.dialog = dialog;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Checkers.restart();
		dialog.dispose();
	}
	
}
