package com.mattc.bugcheckers;

import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;
import info.gridworld.gui.GUIController;
import info.gridworld.gui.GridPanel;
import info.gridworld.gui.WorldFrame;
import info.gridworld.world.World;

import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSlider;

import com.mattc.bugcheckers.TeamManager.Side;
import com.mattc.bugcheckers.actors.Checker;
import com.mattc.bugcheckers.actors.CheckerKing;
import com.mattc.bugcheckers.actors.Highlight;
import com.mattc.bugcheckers.events.KeyFunctionListener;
import com.mattc.bugcheckers.util.Console;
import com.mattc.bugcheckers.util.TerminalThread;
import com.mattc.bugcheckers.util.Utility;
import com.mattc.bugcheckers.world.CheckersWorld;

//TODO Consider Full-Screening, or Definite Sizing the WorldFrame
public class Checkers {

	public static Checkers instance;		//A Recorded Instance of Checkers
	
	public final CheckersWorld world;		//The Current World
	public final TeamManager TEAM_ONE;		//Top Team
	public final TeamManager TEAM_TWO;		//Bottom Team
	public MovementWatcher MOVE_THREAD;		//Thread to Monitor Checker Movement
	public AutoStepper STEP_THREAD;			//Thread to Auto-Step The World
	public VictoryWatcher VICTORY_THREAD;	//Thread to Monitor Victory and Promotion Conditions
	public DisposalWatcher DISPOSAL_THREAD;	//Thread to Ensure Proper Frame Disposal
											//Because Swing is a Burden...
	//Grabbed By Reflection
	@SuppressWarnings("unused")
	private final int ARMY_BITS = Utility.getFieldValue(Ref.class, null, "ARMY_BITS");
	protected volatile static boolean restarting = false;
	
	private Checkers(){
		Console.init();	//Initialize Logging Utility
		Checkers.instance = this;
		world = new CheckersWorld();
		setupWorld(world);
		Ref.checkers = this;
		TEAM_ONE = new TeamManager(Side.TOP, Ref.TOP_COLOR, world);
		TEAM_TWO = new TeamManager(Side.BOTTOM, Ref.BOT_COLOR, world);
		Ref.updateCurrentTeam(TEAM_TWO);					//Bottom Starts
		
		VICTORY_THREAD = new VictoryWatcher(TEAM_ONE, TEAM_TWO);
		VICTORY_THREAD.start();
		
		Ref.frame.repaint();
		GUIHandler.setMessage("Welcome to GridWorld Checkers " + Ref.VERSION + "! ~ Matthew Crocco\nPress Ctrl + R if the 'Debug' Menu Does Not Appear At the Top!");
		Console.misc("Initialization Complete! Have Fun!");
	}
	
	private void setupWorld(World<?> world){
		
		//--Grab WorldFrame Via Reflection to Store in Reference
		Ref.frame = Utility.getFieldValue(World.class, world, "frame");
		//--
		
		//-- GUI Modifications
		WorldFrame wFrame = (WorldFrame) Ref.frame;
		
		wFrame.setTitle(Ref.getFullTitle_WithAuthor());
		wFrame.setResizable(false);
		
		//Basic Key Functions (Escape Exit, Ctrl + R Resetting)
		wFrame.addKeyListener(new KeyFunctionListener());
		
		resizeFrame();
		GridPanel gPanel = Utility.getFieldValue(WorldFrame.class, wFrame, "display"); //Reflection!
		correctGridPanel(gPanel);
		
		Console.debug("SUCCESS!: GridWorld GUI Modified!");
		restarting = false;
		initializeThreads(wFrame, gPanel);
		killGridWorldFeatures();
		addMenus();
	}
	
	/**
	 * Ensures the Window is Resized as the Grid Changes. <br />
	 * Also stores info to Center the Grid
	 */
	private void resizeFrame(){
		if(Ref.DEF_FRAME_DIM == null){
			Ref.DEF_FRAME_DIM = Ref.frame.getSize();
			Ref.FRAME_DIM = Ref.DEF_FRAME_DIM;
		}else{
			Ref.frame.setSize(Ref.FRAME_DIM);
		}
	}
	
	/**
	 * Initialize's Most Threads (VictoryWatcher requires assets outside of this context) <br />
	 * @param wFrame
	 * @param gPanel
	 */
	private void initializeThreads(WorldFrame wFrame, GridPanel gPanel){
		MOVE_THREAD = new MovementWatcher(wFrame, gPanel);
		STEP_THREAD = new AutoStepper();
		DISPOSAL_THREAD = new DisposalWatcher();
		
		//Get Location of Click instead of Scanning for Location
		//Much more efficient. O(1) vs O(n)
		gPanel.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt){
				MOVE_THREAD.processClick(evt.getPoint());
			}
		});
		
		//At Shutdown, Ensure Memory is Released
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				try{
					Checkers.instance.TEAM_ONE.dispose();
					Checkers.instance.TEAM_TWO.dispose();
					Checkers.instance.MOVE_THREAD.terminate();
					Checkers.instance.STEP_THREAD.terminate();
					Checkers.instance.VICTORY_THREAD.terminate();
					Checkers.instance.DISPOSAL_THREAD.terminate();
				}catch(Exception e){}
			}
		}));
		
		//-- Start All Threads Possible
		MOVE_THREAD.start();
		STEP_THREAD.start();
		DISPOSAL_THREAD.start();
		//--
		
		Console.debug("SUCCESS!: Threads Initialized!");
	}
	
	//WARNING! Heavy Reflection Ahead! Kills All Annoying GridWorld Features
	private void killGridWorldFeatures(){
		
		//-- Reflection to Grab GUI Components
		WorldFrame frame = (WorldFrame) Ref.frame;
		GridPanel gPanel = Utility.getFieldValue(WorldFrame.class, frame, "display");
		GUIController control = Utility.getFieldValue(WorldFrame.class, frame, "control");
		JButton stopButton = Utility.getFieldValue(GUIController.class, control, "stopButton");
		JComponent cPanel = Utility.getFieldValue(GUIController.class, control, "controlPanel");
		Component[] cPanelComps = cPanel.getComponents();
		//--
		
		control.run();	//Removes Annoying "Click" Menu's
		Utility.setFieldValue(GUIController.class, control, "running", true);
		
		//--Find and Disable Stop Button and Speed Slider
		for(Component c: cPanelComps){
			if(c instanceof JSlider){
				JSlider speed = (JSlider)c;
				speed.setEnabled(false);
			}
		}
		stopButton.setEnabled(false);
		//--
		
		Console.debug("SUCCESS!: Annoying GridWorld Features Disabled!");
	}
	
	/**
	 * Modify and Add Menu's to MenuBar <br />
	 * <br />
	 * Changes the World Menu and Add's Debug
	 */
	private void addMenus(){
		JMenuBar menu = Ref.frame.getJMenuBar();
		JMenu debug = new JMenu("Debug");
		JMenuItem restart = new JMenuItem("Restart GridWorldCheckers...");
		JMenuItem alList = new JMenuItem("List ActListeners...");
		JMenuItem options = new JMenuItem("Debug Options...");
		JMenuItem help = new JMenuItem("GridWorld Checkers Help...");
		
		//-- Help Menu Item
		help.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GUIHandler.displayHelp();
			}
		});
		//--
		
		//-- Restart Menu Item
		restart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Checkers.restart();
			}
		});
		
		//-- List All ActListeners Item
		alList.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GUIHandler.displayActListenerList(Checkers.instance);
			}
		});
		
		//-- Display Options Modal
		options.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				GUIHandler.displayModificationOptions();
			}
		});
		
		//--Remove Ability to Change Grid and Add Our Items
		menu.getMenu(0).add(restart);
		menu.getMenu(2).add(help);
		((JMenu)menu.getMenu(0).getItem(0)).remove(2);
		((JMenu)menu.getMenu(0).getItem(0)).remove(3);
		((JMenu)menu.getMenu(0).getItem(0)).getItem(0).addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				restart(); //If CheckerGrid appears, Make it Restart Instead of making a new Grid
			}
		});
		((JMenu)menu.getMenu(0).getItem(0)).getItem(0).setToolTipText("Restart's The Game...");
		debug.add(alList);
		debug.add(options);
		//--
		
		//Add to MenuBar
		menu.add(debug);
		
		Console.debug("SUCCESS!: Menu's Added to MenuBar!");
	}
	
	/**
	 * Centers the GridPanel Proportional to the Delta Size of the Window
	 * @param panel
	 */
	private void correctGridPanel(GridPanel panel){
		
		Container contain = panel.getParent();
		contain.setLayout(null);
		
		int offset = 40;
		int diff = Ref.FRAME_DIM.height - Ref.DEF_FRAME_DIM.height;
		offset += diff;
		
		panel.setVisible(false);
		panel.setBounds(contain.getX() + offset, contain.getY() + offset, panel.getWidth(), panel.getHeight());
		panel.setVisible(true);
	}
	
	/**
	 * Dispose All Items and Free Memory then Restart Game
	 */
	public static void restart(){
		restarting = true;
		
		//-- Terminate Threads | THREADS CANT NORMALLY DO THIS! This is my own Subclass of Thread
		//-- NEVER USE Thread.stop()! IT IS UNSAFE!
		Checkers.instance.VICTORY_THREAD.terminate();
		Checkers.instance.MOVE_THREAD.terminate();
		Checkers.instance.STEP_THREAD.terminate();
		//--
		
		//-- Memory Clean-up That The GC May Miss
		Checkers.instance.TEAM_ONE.dispose();
		Checkers.instance.TEAM_TWO.dispose();
		Checkers.instance = null;
		WorldFrame wFrame = (WorldFrame)Ref.frame;
		wFrame.dispose();
		//-- 
		
		new Checkers();
		Console.debug("Restarting...");
	}
	
	public static void main(String[] args){
		new Checkers();
	}
	
}

/**
 * Manages Movement and Highlight Creation
 * 
 * @author Matthew
 */
class MovementWatcher extends TerminalThread{
	
	WorldFrame wFrame;
	GridPanel gPanel;
	
	public MovementWatcher(WorldFrame wFrame, GridPanel gPanel){
		this.wFrame = wFrame;
		this.gPanel = gPanel;
	}
	
	@Override
	public void commit(){
	
		if(Ref.getCurrentTeam() != null && Ref.getCurrentTeam().current != null){
			Highlight.createHighlights(Ref.getCurrentTeam().current);
		}
		
		try{Thread.sleep(200);}catch(InterruptedException e){};
		
	}
	
	/**
	 * Takes a Java AWT Point and Converts it to a Location and Determines Actor Validity
	 * @param p
	 */
	protected void processClick(Point p){
		Location l = gPanel.locationForPoint(p);
		
		Actor a = Checkers.instance.world.getGrid().get(l);
		if(a instanceof Checker){
			//-- If Multi-Jumping
			if(Ref.getCurrentTeam().current != null && Ref.getCurrentTeam().current.hasKilled()) return;
			//--
			
			Checker c = (Checker) a;
			if(c.getTeam().equals(Ref.getCurrentTeam())){
				c.move();
			}
		}
		
		//-- This is where actual Movement happens!
		if(Ref.getCurrentTeam().current != null && a instanceof Highlight){
			Highlight h = (Highlight) a;
			Ref.getCurrentTeam().current.jump(h.getLocation());	//Move To (Determines if Kill Occurs)
			Highlight.clearHighlights();						//Clean Up Highlighted Areas
		
			if(Ref.killOccurred && Ref.getCurrentTeam().current.canJumpAfterKill()){
				Ref.killOccurred = false;
				return;
			}
			
			if(Ref.getCurrentTeam().equals(Checkers.instance.TEAM_ONE)){
				Ref.killOccurred = false;
				Ref.getCurrentTeam().current.toggleKilled();
				Ref.getCurrentTeam().current = null;
				Ref.updateCurrentTeam(Checkers.instance.TEAM_TWO); 
			}else{
				Ref.killOccurred = false;
				Ref.getCurrentTeam().current.toggleKilled();
				Ref.getCurrentTeam().current = null;
				Ref.updateCurrentTeam(Checkers.instance.TEAM_ONE);
			}
		}
		//--
		
	}
	
}

/**
 * Auto-Steps the World
 * 
 * @author Matthew Crocco
 */
class AutoStepper extends TerminalThread{
	
	private World<Actor> world;
	private GridPanel panel;
	
	public AutoStepper(){
		panel = Utility.getFieldValue(WorldFrame.class, (WorldFrame) Ref.frame, "display");
	}
	
	@Override
	public void commit(){
	
		if(world == null){
			if(Checkers.instance != null)
			if(Checkers.instance.world != null){
				world = Checkers.instance.world;
			}
		}else{
			world.step();
			panel.repaint();
		}
		
		try{Thread.sleep(200);}catch(InterruptedException e){}
	}
}

/**
 * Promotes Checkers to CheckerKing's and Declares Victory at Win Condition
 * 
 * @author Matthew Crocco
 */
class VictoryWatcher extends TerminalThread{
	
	private TeamManager Team1, Team2;
	
	public VictoryWatcher(TeamManager Team1, TeamManager Team2){
		this.Team1 = Team1;
		this.Team2 = Team2;
	}
	
	@Override
	public void commit(){
		
		if(!Team1.canContinue()){
			GUIHandler.callVictory(Team2);
		}else if(!Team2.canContinue()){
			GUIHandler.callVictory(Team1);
		}
		
		ArrayList<Checker> kings = getCheckersToKing();
		
		for(Checker c: kings){
			if(c instanceof CheckerKing) continue;
			c.promote();
		}
			
		try{Thread.sleep(200);}catch(InterruptedException e){}
	}
	
	private ArrayList<Checker> getCheckersToKing(){
		ArrayList<Checker> tmp = new ArrayList<Checker>();
		Grid<Actor> grid = Checkers.instance.world.getGrid();
		int[] rows = {0, grid.getNumRows()-1};
		int width = Checkers.instance.world.getGrid().getNumCols();
		
		for(int i = 0; i < 2; i++){
			for(int c = 0; c < width;c++){
				Actor a = grid.get(new Location((i == 0 ? 0 : grid.getNumRows()-1), c));
				if(a instanceof Checker && !(a instanceof CheckerKing)){
					Checker check = (Checker) a;
					int home = check.getTeam().getHomeRow();
					if(check.getLocation().getRow() != home){
						tmp.add(check);
					}
				}
			}
		}
		
		return tmp;
	}
	
}

/**
 * Watches The WorldFrame to Ensure that it is Disposed of Properly (Preventing The Most Common Memory Leak) <br />
 * <br />
 * This is only because I use Reflection to modify the Frame. Otherwise I would edit the source code to fix this myself.
 * 
 * @author Matthew Crocco
 */
class DisposalWatcher extends TerminalThread{
	
	public void commit(){
		if(!Ref.frame.isVisible() && !Checkers.restarting){
			Runtime.getRuntime().exit(0);
		}
	}
	
}