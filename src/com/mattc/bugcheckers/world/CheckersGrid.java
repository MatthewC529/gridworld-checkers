package com.mattc.bugcheckers.world;

import info.gridworld.actor.Actor;
import info.gridworld.grid.BoundedGrid;

import com.mattc.bugcheckers.Ref;

/**
 * A Simple Grid Subclass that creates a Checker Grid (8x8 by default)
 * @author Matthew Crocco
 *
 */
public class CheckersGrid extends BoundedGrid<Actor>{

	public CheckersGrid() {
		super(Ref.GRID_SIZE, Ref.GRID_SIZE);
	}
	
}
