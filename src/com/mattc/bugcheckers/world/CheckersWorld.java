package com.mattc.bugcheckers.world;

import info.gridworld.actor.ActorWorld;

import java.util.ArrayList;
import java.util.List;

import com.mattc.bugcheckers.events.ActListener;

/**
 * An Implementation of ActorWorld that permits ActListeners (Allowing things to act onStep) <br />
 * <br />
 * Also gives me more immediate control of the World.
 * 
 * @author Matthew Crocco
 */
public class CheckersWorld extends ActorWorld{

	//A List of All Act Listeners
	public List<ActListener> actListeners;
	
	/**
	 * Default Constructor <br />
	 * <br />
	 * Auto Creates a Checker Board Grid and Allows ActListening!
	 */
	public CheckersWorld(){
		super(new CheckersGrid());
		actListeners = new ArrayList<ActListener>();
		show();
	}
	
	/**
	 * Minor Override that Allows Classes to Listen for Steps
	 */
	@Override
	public void step(){
		super.step();
		for(ActListener l: actListeners){
			l.onAct();
		}
	}
	
	/**
	 * Add ActListener to listen for Step Events
	 * @param listener
	 */
	public void addActListener(ActListener listener){
		actListeners.add(listener);
	}
	
	/**
	 * Remove ActListener, terminating its listening.
	 * @param listener
	 */
	public void removeActListener(ActListener listener){
		actListeners.remove(listener);
	}
	
	/**
	 * Remove All Act Listeners
	 */
	public void clearActListener(){
		actListeners.clear();
	}
	
	public ActListener[] getActListeners(){
		return actListeners.toArray(new ActListener[actListeners.size()]);
	}
	
}
