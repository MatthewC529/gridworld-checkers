package com.mattc.bugcheckers;

import java.awt.Dimension;

import javax.swing.JFrame;

import com.mattc.bugcheckers.util.Colour;
import com.mattc.bugcheckers.util.Console;

/**
 * A Stand-Alone Reference File <br />
 * <br />
 * Store's State Information
 * 
 * @author Matthew Crocco
 */
public final class Ref {

	private Ref(){}
	
	public static final String AUTHOR = "Matthew Crocco";			//AUTHOR
	public static final String GAME_TITLE = "GridWorld Checkers";	//TITLE
	public static final String VERSION = "v0.7.0a";					//VERSION
	public static final int DEF_GSIZE = 8;							//Default Grid Size
	public static int REF_GRID_SIZE = DEF_GSIZE;
	public static int GRID_SIZE = DEF_GSIZE;						//Current Grid Size
	public static Checkers checkers;								//Checkers Instance
	public static JFrame frame;										//Current WorldFrame
	public static volatile boolean killOccurred = false;			//If Currnet Checker has Killed
	public static final int DEF_ABITS = 0x03;						//Default Layers (Hexadecimal)
	public static volatile int ARMY_BITS = DEF_ABITS;				//Current Layers (Decimal)
	
	public static final Colour DEF_TCOLOR = Colour.findColour("black");	//Default Top Colour
	public static final Colour DEF_BCOLOR = Colour.findColour("red");	//Default Bottom Colour
	public static volatile Colour TOP_COLOR = DEF_TCOLOR;				//Current Top Colour
	public static volatile Colour BOT_COLOR = DEF_BCOLOR;				//Current Bottom Colour
	
	public static Dimension DEF_FRAME_DIM;							//Default Frame Dimensions (8x8 Board) -- Benchmark
	public static Dimension REF_FRAME_DIM;
	public static Dimension FRAME_DIM;								//Current Frame Dimensions (Up to 12x12 Board)
	
	private static TeamManager currentTeam;							//Current Team's Turn
	
	/**
	 * Update Current Team
	 * @param team
	 */
	public static void updateCurrentTeam(TeamManager team){
		Ref.currentTeam = team;
		Checkers.instance.world.setMessage(team.getSide() + " -- It is your turn!");
		Console.debug("Team Updated to " + team.getSide());
	}
	
	/**
	 * Get Current Team whose Turn it is.
	 * @return
	 */
	public static TeamManager getCurrentTeam(){
		return Ref.currentTeam;
	}
	
	/**
	 * Dispose of GridWorld Assets
	 */
	public static void terminateGridWorld(){
		Ref.frame.dispose();
		Runtime.getRuntime().exit(0);
	}
	
	/**
	 * Get Full Title without My Awesome Name
	 * @return
	 */
	public static final String getFullTitle_NoAuthor(){
		return String.format("%s - %s", GAME_TITLE, VERSION);
	}
	
	/**
	 * Get Full Title WITH My Awesome Name!
	 * @return
	 */
	public static final String getFullTitle_WithAuthor(){
		return String.format("%s - %s | By %s", GAME_TITLE, VERSION, AUTHOR);
	}
	
	//Ridiculous
	private static final void func_133764123bai(){
		Ref.GRID_SIZE = 15;
		Ref.ARMY_BITS = 5;
		Ref.FRAME_DIM = new Dimension(790, 720);
		Checkers.restart();
	}
	
}
