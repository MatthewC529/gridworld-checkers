package com.mattc.bugcheckers.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * A Logging Utility that prints out to the Console with the Time and Log Level <br />
 * <br />A Simple Debugging Tool.
 * 
 * @author Matthew Crocco
 */
public final class Console {
	
	/**
	 * Log Level
	 * @author Matthew Crocco
	 */
	public enum Level{
		
		EXCEPTION,
		FATAL,
		WARNING,
		DEBUG,
		INFO,
		MISC;
		
	}
	
	private static SimpleDateFormat date;
	private static boolean _init = false;	//Prevents Redundant Initialization
	
	private Console(){}
	
	public static void init(){
		if(_init) return;
		SimpleDateFormat tmp = new SimpleDateFormat("MM/dd/yyyy");
		date = new SimpleDateFormat("HH:mm:ss");
		log(String.format("Logger Initialized For GridWorldCheckers on %s!", tmp.format(new Date())));
		_init = true;
	}
	
	/**
	 * Print to Console as LOGGER_MSG
	 * @param msg
	 */
	private static void log(Object msg){
		System.out.printf("(%s)[%s] -- %s\n", date.format(new Date()), "LOGGER_MSG", msg);
	}
	
	public static void log(Object msg, Level level){
		Date d = new Date();
		if(level == Level.EXCEPTION)
			System.err.printf("(%s)[%s] -- %s\n", date.format(d), level.name(), msg);
		else
			System.out.printf("(%s)[%s] -- %s\n", date.format(d), level.name(),  msg);
	}
	
	public static void fatal(Object msg){
		log(msg, Level.FATAL);
	}
	
	public static void error(Object msg){
		log(msg, Level.FATAL);
	}
	
	public static void warn(Object msg){
		log(msg, Level.WARNING);
	}
	
	public static void info(Object msg){
		log(msg, Level.INFO);
	}
	
	public static void debug(Object msg){
		log(msg, Level.DEBUG);
	}
	
	public static void misc(Object msg){
		log(msg, Level.MISC);
	}
	
	public static void superfluous(Object msg){
		log(msg, Level.MISC);
	}
	
	/**
	 * Prints an Exception Stack Trace to the Console in a Clean Way
	 * @param e
	 */
	public static void exception(Exception e){
		StackTraceElement[] elements = e.getStackTrace();
		exception("-------EXCEPTION CAUGHT-------");
		
		exception("MESSAGE: " + e.getLocalizedMessage());
		for(StackTraceElement element : elements){
			exception(element);
		}
		
		exception("-------EXCEPTION CAUGHT-------");
	}
	
	private static void exception(Object msg){
		log(msg, Level.EXCEPTION);
	}
	
}
