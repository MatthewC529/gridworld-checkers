package com.mattc.bugcheckers.util;

import java.lang.reflect.Array;

/**
 * A Toolkit of Various Array Utilities that are used throughout this Program <br />
 * <br />
 * Including Generic Array Creation and Array Editing Methods.
 * 
 * @author Matthew
 *
 */
public final class ArrayUtils {

	private ArrayUtils(){}
	
	/**
	 * Dereferences (Reference Type -> Value Type) an Integer Array into an int Array
	 * @param arr
	 * @return
	 */
	public static int[] dereferenceArray(Integer[] arr){
		int[] tmp = new int[arr.length];
		
		for(int i = 0 ; i < tmp.length; i++){
			tmp[i] = arr[i];
		}
		
		return tmp;
	}
	
	/**
	 * Removes an Object from an Array
	 * @param arr
	 * @param obj
	 * @return
	 */
	public static <T> T[] removeFromArray(T[] arr, T obj){
		for(int i = 0; i < arr.length; i++){
			if(arr[i] == obj)
				return removeFromArray(arr, i);
		}
		
		return arr;
	}
	
	/**
	 * Removes an Object at a Specific Index from an Array
	 * @param arr
	 * @param index
	 * @return
	 */
	public static <T> T[] removeFromArray(T[] arr, int index){
		
		T[] tmp = ArrayUtils.createArray(arr.getClass().getComponentType(), arr.length-1);
		int tIndex = 0;
		for(int i = 0; i < arr.length; i++){
			if(i == index) continue;
			
			tmp[tIndex++] = arr[i];
		}
		
		arr = tmp;
		return arr;
	}
	
	/**
	 * Create New One-Dimensional Array Using Reflection Tools
	 * @param clazz
	 * @param size
	 * @return
	 */
	public static <T> T[] createArray(Class<?> clazz, int size){
		return (T[]) Array.newInstance(clazz, size);
	}
	
	/**
	 * Create New Two-DImensional Array Using Reflection Tools
	 * @param clazz
	 * @param sizeOne
	 * @param sizeTwo
	 * @return
	 */
	public static <T> T[][] createArray(Class<?> clazz, int sizeOne, int sizeTwo){
		return (T[][]) Array.newInstance(clazz, sizeOne, sizeTwo);
	}
	
}
