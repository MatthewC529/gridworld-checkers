package com.mattc.bugcheckers.util;

import java.awt.Color;

/**
 * A Variation of Colour that Supports Hexadecimal and Floats from 0.0 -> 1.0 <br />
 * <br />
 * Also provides a more detailed toString Method and the ability to ocnvert Color's to Colour's. <br />
 * Besides, Colour is fancy!
 * @author Matthew Crocco
 *
 */
public final class Colour extends Color{
	
	private static final long serialVersionUID = 9042827464189265383L;
	
	private float r = -1, g = -1, b = -1, a = -1;
	private String name = "-";
	
	public Colour(Color c){
		this(c.getRed()/255f, c.getGreen()/255f, c.getBlue()/255f, c.getAlpha()/255f);
	}
	
	public Colour(Color c, String name){
		this(c);
		this.name = name;
	}
	
	public Colour(float r, float g, float b, float a) {
		super(r, g, b, a);
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public static Colour getColour(String hex, float a){
		
		float r, g, b;
	
		String rStr = hex.substring(0, 2);
		String gStr = hex.substring(2, 4);
		String bStr = hex.substring(4, 6);
		
		r = Integer.parseInt(rStr, 16) / 255.0f;
		g = Integer.parseInt(gStr, 16) / 255.0f;
		b = Integer.parseInt(bStr, 16) / 255.0f;
		
		if(hex.length() >= 8)
			a = Integer.parseInt(hex.substring(6, 8), 16) / 255.0f;
		else
			a = 1.0f;
		
		return new Colour(r, g, b, a);
		
	}
	
	public static Colour getColour(String hex){
		if(hex.length() <= 6) return getColour(hex, 1.0f);
		
		float alpha = Integer.parseInt(hex.substring(6, 8), 16) / 255.0f;
		
		return getColour(hex.substring(0, 6), alpha);
	}
	
	public static Colour findColour(String name){
		
		Colour c = new Colour((Color) Utility.getFieldValue(Color.class, null, name));
		
		if(c == null)
			return new Colour(Color.WHITE);
		else
			return c;
	}
	
	public String getAsHexString(){
		int r, g, b, a;
		
		if(this.r == -1 || this.g == -1 || this.b == -1 || this.a == -1){
			r = this.getRed();
			g = this.getBlue();
			b = this.getGreen();
			a = this.getAlpha();
		}else{
			r = (int) (this.r * 255);
			g = (int) (this.g * 255);
			b = (int) (this.b * 255);
			a = (int) (this.a * 255);
		}
		
		String hex1 = hexPad(Integer.toHexString(r));
		String hex2 = hexPad(Integer.toHexString(g));
		String hex3 = hexPad(Integer.toHexString(b));
		String hex4 = hexPad(Integer.toHexString(a));
		
		return hex1 + hex2 + hex3 + hex4;
	}
	
	private String hexPad(String hex){
		if(hex.length() < 2){
			return "0"+hex;
		}
		
		return hex;
	}
	
	@Override
	public String toString(){
		float r, g, b, a;
		String type = "-";
		
		if(this.r == -1 || this.g == -1 || this.b == -1 || this.a == -1){
			r = this.getRed();
			g = this.getBlue();
			b = this.getGreen();
			a = this.getAlpha();
			type = "Color";
		}else{
			r = this.r * 255;
			g = this.g * 255;
			b = this.b * 255;
			a = this.a * 255;
			type = "Colour";
		}
		
		return String.format("Name=%s        {Hex=%s RGBA=[%s, %s, %s, %s] Type=%s}",name == null ? "-" : Utility.toDisplayName(name), "#"+getAsHexString().toUpperCase(), r, g, b, a, type); 
	}
}
